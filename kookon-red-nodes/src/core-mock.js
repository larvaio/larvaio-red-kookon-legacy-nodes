/* eslint-disable class-methods-use-this */
/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
module.exports = class CoreMock {
  add(...args) {
    console.warn('CORE MOCK ADDED');
  }

  async report(...args) {
    console.warn('CORE MOCK REPORTED');
  }

  async send(...args) {
    console.warn('CORE MOCK SEND');
  }
};
