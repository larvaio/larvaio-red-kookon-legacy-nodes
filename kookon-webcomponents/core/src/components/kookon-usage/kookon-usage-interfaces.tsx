export interface UtilityValue {
  abs: string | number, rel: string | number , date: string
}

export interface UtilityMeterState {
  water?: UtilityValue[];
  electricity?: UtilityValue[];
}
