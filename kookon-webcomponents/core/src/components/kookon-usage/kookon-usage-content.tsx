import { Component, Prop, h } from '@stencil/core';
import { LogEntry } from './kookon-usage-log';
import moment from 'moment';

@Component({
  tag: 'kookon-usage-content',
  styleUrl: 'kookon-usage.scss',
  shadow: false,
  scoped: true
})
export class KookonUsageContent {
   @Prop() waterMonths?: any[];
   @Prop() electrictyMonths?: any[];

   render() {
    let latestMonthWater = this.waterMonths.length > 0 ? this.waterMonths[0] : null;
    let latestMonthElectricity = this.electrictyMonths.length > 0 ? this.electrictyMonths[0] : null;
    if (latestMonthWater && latestMonthWater.date !== moment().format('MM-YYYY')) {
      latestMonthWater = null;
    }
    if (latestMonthElectricity && latestMonthElectricity.date !== moment().format('MM-YYYY')) {
      latestMonthElectricity = null;
    }
    const entriesWater: LogEntry[] = this.waterMonths.map(month => {
      return {
        start: (
          <div style={{ color: 'var(--lar-color-secondary)', fontSize:'.85rem' }}>
            <lar-translate style={{ fontWeight: 'bold' }} t={'date.months.' + (moment(month.date, 'MM-YYYY').month() + 1)} />
            <br />
            <small>{moment(month.date, 'MM-YYYY').year()}</small>
          </div>
        ),
        content: '',
        end: (
          <div style={{ paddingTop: '.4rem' }}>
            <small>{Number(month.rel).toFixed(1)}</small>
            <small>m<sup>3</sup></small>
          </div>
        ),
      };
    });
    const entriesElectricity: LogEntry[] = this.electrictyMonths.map(month => {
      return {
        start: (
          <div style={{ color: 'var(--lar-color-secondary)', fontSize:'.85rem' }}>
            <lar-translate style={{ fontWeight: 'bold' }} t={'date.months.' + (moment(month.date, 'MM-YYYY').month() + 1)} />
            <br />
            <small>{moment(month.date, 'MM-YYYY').year()}</small>
          </div>
        ),
        content: '',
        end: (
          <div style={{ paddingTop: '.4rem' }}>
            <small>{Number(month.rel).toFixed(0)}</small>
            <small>kWh</small>
          </div>
        ),
      };
    });
    return (
      <div>
      <div class="grid">
      <div class="col">
      {latestMonthElectricity &&
        <div class="header">
          <h3>
          <lar-translate t="kookon.electricity" />
          </h3>
            <span class="light">
            <lar-translate t="kookon.currentMonth" />
            </span>
          <h2>
          {Number(latestMonthElectricity.rel).toFixed(0)}
            kWh
          </h2>
        </div>
      }
      <kookon-usage-log entries={entriesElectricity} />
      </div>
      <div class="col">
        {latestMonthWater &&
          <div class="header">
            <h3>
            <lar-translate t="kookon.water" />
            </h3>
              <span class="light">
                <lar-translate t="kookon.currentMonth" />
              </span>
            <h2>
            {Number(latestMonthWater.rel).toFixed(1)}
              m<sup>3</sup>
            </h2>
          </div>
        }
        <kookon-usage-log entries={entriesWater} />
      </div>
    </div>
    </div>
    );
  }
}
