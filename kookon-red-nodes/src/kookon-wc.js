module.exports = function RegisterKookon(RED) {
  class Kookon {
    constructor(config) {
      this.RED = RED;
      this.RED.nodes.createNode(this, config); // this will extend with EventEmitter
      this.id = config.id;
      this.plc = RED.nodes.getNode(config.plc);
      if (this.plc) {
        this.plc.register(this);
      } else {
        this.setError('PLC config missing');
      }
      this.on('input', this.handleInput.bind(this));
    }

    async handleInput(msg, send, done) {
      try {
        if (!this.plc) {
          throw new Error('Not Connected');
        } else {
          await this.plc.write(`.bToiletDoorOpen`, true);
          this.status({ fill: 'green', shape: 'dot', text: 'Success' });
        }
        done();
      } catch (err) {
        this.setError(err.message);
        done(err);
      }
    }

    setError(err) {
      this.status({ fill: 'red', shape: 'ring', text: err });
    }
  }

  RED.nodes.registerType('kookon-wc', Kookon);
};
