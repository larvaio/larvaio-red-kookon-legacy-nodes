import { Component, Prop, h, Event, EventEmitter } from '@stencil/core';

@Component({
  tag: 'kookon-area-content',
  shadow: false,
  scoped: false
})
export class KookonAreaContent {
   @Prop() color?: string;
   @Prop() loading?: boolean;
   @Prop() zoneopened?: boolean;
   @Prop() ready?: boolean;
   @Prop() armed?: boolean;
   @Prop() alarm?: boolean;
   @Event() arm!: EventEmitter;
   @Event() disarm!: EventEmitter;

   render() {
    return (
      <div class="actions">
            {this.zoneopened
            ? <h3><lar-translate t="error_messages.AREA_ARM_FAILED_INVALID_ZONE_STATE" /></h3>
            : <h3>&nbsp;</h3>
            }
          {(this.armed || this.alarm)
            ?
            <lar-button
              disabled={this.loading || !this.ready}
              onClick={() => this.disarm.emit()}
              color={this.color}
            >
              <lar-icon icon="unlock" /> <lar-translate t="area.disarm"/>
            </lar-button>
            :
            <lar-button-group>
              <lar-button
                disabled={this.loading || !this.ready || this.zoneopened}
                onClick={() => this.arm.emit()}
                color={this.color}
              >
                <lar-icon icon="lock" /> <lar-translate t="area.arm"/>
              </lar-button>
            </lar-button-group>
          }
        </div>
    );
  }
}
