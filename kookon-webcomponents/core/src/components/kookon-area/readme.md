# my-component



<!-- Auto Generated Below -->


## Properties

| Property     | Attribute    | Description | Type      | Default     |
| ------------ | ------------ | ----------- | --------- | ----------- |
| `alarm`      | `alarm`      |             | `boolean` | `undefined` |
| `armed`      | `armed`      |             | `boolean` | `undefined` |
| `color`      | `color`      |             | `string`  | `undefined` |
| `loading`    | `loading`    |             | `boolean` | `undefined` |
| `ready`      | `ready`      |             | `boolean` | `undefined` |
| `zoneopened` | `zoneopened` |             | `boolean` | `undefined` |


## Events

| Event    | Description | Type               |
| -------- | ----------- | ------------------ |
| `arm`    |             | `CustomEvent<any>` |
| `disarm` |             | `CustomEvent<any>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
