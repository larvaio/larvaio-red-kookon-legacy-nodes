const INTERVAL = 50000;
const MAX_CARDS = 20; // maxACLCardLen
const ARR_START = 1; // plc array start
const CoreBaseNode = require('./core-base-node');

module.exports = function RegisterKookon(RED) {
  class KookonCards extends CoreBaseNode {
    constructor(config) {
      super(RED, config);
      this.unit = config.unit;
      this.changed = true;
      this.pollCards = this.pollCards.bind(this);
      this.cards = new Array(MAX_CARDS);
      for (let i = ARR_START; i <= MAX_CARDS; i += 1) {
        this.cards[i] = { SCARDID: '', BYSTATUS: 0 };
      }
      this.boxPLCID = parseInt(config.boxPLCID, 10);
      this.plc = RED.nodes.getNode(config.plc);
      if (this.plc && !Number.isNaN(this.boxPLCID)) {
        this.plc.register(this);
        this.plc.on('connected', this.pollCards);
        const randomInterval = Math.floor(
          Math.random() * (INTERVAL + 3000 - (INTERVAL - 3000) + 1) + (INTERVAL - 3000)
        );
        this.interval = setInterval(this.pollCards, randomInterval);
      } else {
        this.setError('PLC config invalid/missing');
      }

      this.on('close', async (done) => {
        clearInterval(this.interval);
        this.plc.off('connected', this.pollCards);
        done();
      });
    }

    async pollCards() {
      try {
        this.status({ fill: 'yellow', shape: 'ring', text: 'Requesting...' });
        await new Promise((resolve) => setTimeout(resolve, 200));
        const { payload } = await this.core.request({
          topic: 'iot-2/cmd/getUnitCards/fmt/json',
          payload: {
            data: {
              id: this.unit,
            },
          },
        });
        const { data } = payload;
        if (data && data.cards && Array.isArray(data.cards)) {
          const { cards } = data;
          for (let i = ARR_START; i <= MAX_CARDS; i += 1) {
            const card = cards[i - ARR_START] ? cards[i - ARR_START] : undefined;
            if (card) {
              card.validranges = (card.validranges || []).map((range) => {
                const start = !range[0] ? 0 : new Date(range[0]).getTime() / 1000;
                const end =
                  range[1] === 'Infinity' ? Infinity : new Date(range[1]).getTime() / 1000;
                return [start, end];
              });
              if (card.validranges.length === 0) {
                card.validranges.push([null, Infinity]);
              }
              const now = new Date().getTime() / 1000;
              card.isActive = !!card.validranges.find(
                (range) => Array.isArray(range) && range[0] <= now && range[1] >= now
              );
            }
            //
            const cardId =
              card && card.card ? KookonCards.convertTo14ByteDecString(card.card) : undefined;
            if (
              card &&
              cardId &&
              (this.cards[i].SCARDID !== cardId || this.cards[i].BYSTATUS !== Number(card.isActive))
            ) {
              this.changed = true;
              this.cards[i] = { SCARDID: cardId, BYSTATUS: Number(card.isActive) };
            } else if (!card && this.cards[i].BYSTATUS) {
              this.changed = true;
              this.cards[i] = { SCARDID: '', BYSTATUS: 0 };
            }
          }
          if (this.changed === true) {
            this.status({ fill: 'yellow', shape: 'ring', text: 'Write to plc...' });
            await this.writePLC(this.cards);
            this.changed = false;
          } else {
            this.status({ fill: 'green', shape: 'ring', text: 'Up 2 date' });
          }
          this.status({ fill: 'green', shape: 'dot', text: 'Done' });
        } else {
          throw new Error('Invalid Core Response');
        }
      } catch (err) {
        this.setError(err.message);
      }
    }

    async writePLC(cards) {
      if (!this.plc || !this.boxPLCID) {
        throw new Error('Not Connected to PLC');
      } else {
        await this.plc.write(`.starrStorages[${this.boxPLCID}].arrACLCard`, cards);
        this.status({ fill: 'green', shape: 'dot', text: 'PLC Write Success' });
      }
    }

    static convertTo14ByteDecString(input) {
      const hex = Buffer.from(input.replace(/^0x/, ''), 'hex');
      let ret;
      /* workaround for already inserted 6bytes hex */
      if (hex.length === 6) {
        ret = hex.join('').slice(0, 14);
      } else {
        ret = hex.subarray(1).join('').slice(0, 14);
      }
      return ret;
    }
  }

  RED.nodes.registerType('kookon-cards', KookonCards);
};
