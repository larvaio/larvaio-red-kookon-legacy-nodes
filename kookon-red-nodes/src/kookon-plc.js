/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-console */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-param-reassign */
/* eslint-disable class-methods-use-this */
const Beckhoff = require('beckhoff-js');

module.exports = function RegisterPLCHandler(RED) {
  class KookonPLCConfig {
    constructor(config) {
      this.nodes = [];
      this.RED = RED;
      this.config = config;
      this.ready = false;
      RED.nodes.createNode(this, config); // this will extend with EventEmitter
      this.closeClient = this.closeClient.bind(this);
      this.registerNodesNotifyHandlers = this.registerNodesNotifyHandlers.bind(this);
      this.registerNodeNotifyHandlers = this.registerNodeNotifyHandlers.bind(this);
      this.writeNodeDefaults = this.writeNodeDefaults.bind(this);
      if (!config.host) {
        this.status({ fill: 'red', shape: 'ring', text: 'Host missing' });
      } else {
        // beckhoff is wery sensitive regarding lose ended notify handlers
        const sigTermHandler = () => process.emit('beforeExit', 16);
        const exitHandler = async (code) => {
          console.warn(`beckhoff-js: Process exit(${code}), closing Beckhoff client`);
          await this.closeClient();
          process.exit(code);
        };
        process.on('SIGUSR1', sigTermHandler);
        process.on('SIGUSR2', sigTermHandler);
        process.on('SIGTERM', sigTermHandler);
        process.on('SIGQUIT', sigTermHandler);
        process.on('SIGINT', sigTermHandler);
        process.on('beforeExit', exitHandler);
        this.on('close', async (done) => {
          process.off('SIGUSR1', sigTermHandler);
          process.off('SIGUSR2', sigTermHandler);
          process.off('SIGTERM', sigTermHandler);
          process.off('SIGQUIT', sigTermHandler);
          process.off('SIGINT', sigTermHandler);
          process.off('beforeExit', exitHandler);
          done();
        });
      }
    }

    async closeClient() {
      if (this.client) {
        const { client } = this;
        this.client = undefined;
        await client
          .close()
          .then(() => console.warn('beckhoff-js: Client closed (also all handles removed)....'))
          .catch((err) => console.error('beckhoff-js: ', err));
        client.removeAllListeners();
      }
    }

    async openClient() {
      try {
        if (this.client) {
          return;
        }
        this.status({ fill: 'yellow', shape: 'ring', text: 'Opening...' });
        const options = {
          target: {
            host: this.config.host,
            netID: this.config.targetNetID,
            amsPort: parseInt(this.config.amsPort || 801, 10),
          },
          source: {
            netID: this.config.sourceNetID,
          },
          loadSymbols: true,
          loadDataTypes: true,
          reconnect: true,
          reconnectInterval: 10000,
        };
        if (!options.target.host) {
          throw new Error('PLC Host missing');
        }
        this.status({ fill: 'yellow', shape: 'dot', text: 'Connecting...' });
        // eslint-disable-next-line new-cap
        this.client = new Beckhoff.default(options);
        this.client.on('error', (err) => {
          this.status({ fill: 'red', shape: 'ring', text: err.message });
        });
        this.client.on('connected', () => {
          this.status({ fill: 'green', shape: 'dot', text: 'Connected' });
        });
        this.client.on('close', () => {
          this.status({ fill: 'red', shape: 'ring', text: 'Closed' });
        });
        this.client.on('reconnect', () => {
          this.status({ fill: 'yellow', shape: 'dot', text: 'Re-Connecting...' });
        });
        await this.client.connect();
        // wait for next tick so all nodes are registered
        await new Promise((resolve) => setImmediate(resolve));
        await this.registerNodesNotifyHandlers();
      } catch (err) {
        this.status({ fill: 'red', shape: 'ring', text: err.message });
      }
    }

    async read(tagName) {
      if (!this.connected || !this.ready) {
        throw new Error('Client not connected');
      }
      return this.client.readTag(tagName);
    }

    async write(tagName, value) {
      if (!this.connected || !this.ready) {
        throw new Error('Client not connected');
      }
      return this.client.writeTag(tagName, value);
    }

    status(status) {
      this.statusMessage = status;
      this.nodes.forEach((node) => {
        node.status(status);
      });
    }

    async register(node) {
      try {
        if (this.statusMessage) {
          node.status(this.statusMessage);
        }
        this.nodes.push(node);
        node.on('close', async (done) => {
          await this.deregister(node);
          done();
        });
        if (!this.client) {
          await this.openClient();
        }
      } catch (err) {
        node.setError(err.message);
      }
    }

    async registerNodeNotifyHandlers(node = {}) {
      const handlers = node.notifyHandlers;
      if (!!handlers && Array.isArray(handlers)) {
        for (const handler of handlers) {
          if (!this.connected) {
            return;
          }
          await this.client
            .monitorTag(handler.tagName, handler.handler)
            .catch((err) => node.status({ fill: 'red', shape: 'ring', text: err.message }));
        }
      }
    }

    async writeNodeDefaults(node = {}) {
      const handlers = node.writeDefaultValues;
      if (!!handlers && Array.isArray(handlers)) {
        for (const handler of handlers) {
          if (!this.connected) {
            return;
          }
          await this.client
            .writeTag(handler.tagName, handler.value)
            .catch((err) => node.status({ fill: 'red', shape: 'ring', text: err.message }));
        }
      }
    }

    async registerNodesNotifyHandlers() {
      for (const node of this.nodes) {
        await this.registerNodeNotifyHandlers(node);
        await this.writeNodeDefaults(node);
      }
      this.ready = true;
      this.emit('connected');
    }

    async deregister(node) {
      try {
        const index = this.nodes.indexOf(node);
        if (index !== -1) {
          this.nodes.splice(index, 1);
        }
        const handlers = node.notifyHandlers;
        if (!!handlers && Array.isArray(handlers)) {
          for (const handler of handlers) {
            if (!this.connected) {
              return;
            }
            await this.client
              .stopMonitorTag(handler.tagName)
              .catch((err) => node.status({ fill: 'red', shape: 'ring', text: err.message }));
          }
        }
        if (this.nodes.length === 0) {
          await this.closeClient(); // close client removes all notify handler automatically
        }
      } catch (err) {
        node.setError(err.message);
      }
    }

    get connected() {
      return this.client && this.client.connected;
    }
  }

  RED.nodes.registerType('kookon-plc', KookonPLCConfig);
};
