import { App, Plugin } from 'vue';
import { applyPolyfills, defineCustomElements } from '@larva.io/k-webcomponents/loader';

export * from './proxies';
export * from '@larva.io/k-webcomponents';

const ael = (el: any, eventName: string, cb: any, opts: any) => el.addEventListener(eventName.toLowerCase(), cb, opts);
const rel = (el: any, eventName: string, cb: any, opts: any) => el.removeEventListener(eventName.toLowerCase(), cb, opts);

export const LarvaWebcomponents: Plugin = {
  async install(_app: App) {
    if (typeof (window as any) !== 'undefined') {
      await applyPolyfills();
      await defineCustomElements(window, {
        exclude: [],
        ce: (eventName: string, opts: any) => new CustomEvent(eventName.toLowerCase(), opts),
        ael,
        rel
      } as any);
    } else {
      throw new Error('Missing Window object. Are you in browser?');
    }
  }
};
