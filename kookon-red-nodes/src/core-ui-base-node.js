const _ = require('lodash');
const CoreBaseNode = require('./core-base-node');

module.exports = class CoreUiBaseNode extends CoreBaseNode {
  constructor(RED, config, log = false) {
    super(RED, config);
    const { units } = config;
    this.topic = `iot-2/evt/${this.id}/fmt/json`;
    this.core.add({
      node: this,
      ui: { ...this.getUIFromNodeConfig(config), log },
      units,
    });
  }

  getUIFromNodeConfig(config) {
    const params = ['name', 'visible', 'favorite', 'rating', 'icon', 'room', 'category'];
    const ui = _.chain(config).pick(params).value();
    if (ui.room) {
      ui.room = _.pick(this.RED.nodes.getNode(ui.room) || {}, params);
    } else {
      ui.room = {};
    }
    if (ui.category) {
      ui.category = _.pick(this.RED.nodes.getNode(ui.category) || {}, params);
    } else {
      ui.category = {};
    }
    return ui;
  }
};
