# kookon-usage-log



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description | Type         | Default     |
| --------- | --------- | ----------- | ------------ | ----------- |
| `entries` | --        |             | `LogEntry[]` | `undefined` |


## Dependencies

### Used by

 - [kookon-usage-content](.)

### Graph
```mermaid
graph TD;
  kookon-usage-content --> kookon-usage-log
  style kookon-usage-log fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
