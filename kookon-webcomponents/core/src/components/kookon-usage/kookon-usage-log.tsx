import {
  Component,
  Prop,
  h
} from '@stencil/core';

export interface LogEntry {
  start: string;
  content: string;
  end: string;
}

@Component({
  tag: 'kookon-usage-log',
  styleUrl: 'kookon-usage-log.scss',
  shadow: true
})
export class LogEntryList {
  @Prop() entries: LogEntry[];
  render() {
    return (
      <lar-list>
        {this.entries.map(entry => {
          return (
            <div>
              <lar-list-item>
                <div slot="start">{entry.start}</div>
                <div>{entry.content}</div>
                <div slot="end">{entry.end}</div>
              </lar-list-item>
            </div>
          );
        })}
      </lar-list>
    );
  }
}
