const _ = require('lodash');
const Boom = require('@hapi/boom');
const CoreUiBaseNode = require('./core-ui-base-node');
const KookonUsageData = require('./kookon-usage-data');

module.exports = function RegisterKookon(RED) {
  class KookonUsage extends CoreUiBaseNode {
    constructor(config) {
      super(RED, config);
      this.waterContextData = new KookonUsageData({
        context: this.context(),
        key: 'water',
      });
      this.electicityContextData = new KookonUsageData({
        context: this.context(),
        key: 'electicity',
      });
      this.updateStatus();
    }

    updateStatus() {
      this.status({
        fill: 'green',
        text: `${this.waterContextData.getAbsValue()}m3 / ${this.electicityContextData.getAbsValue()}kWh`,
      });
    }

    /**
     * Get the full state context with all monthly data
     *
     * @readonly
     * @memberof UtilityMeter
     */
    get state() {
      return {
        water: this.waterContextData.getAllMonths(),
        electricity: this.electicityContextData.getAllMonths(),
      };
    }

    /**
     * Save the incoming absolute value in the contextData
     *
     * @param {*} { abs, dateKey }
     * @memberof UtilityMeter
     */
    setWaterAbsValue({ abs, dateKey }) {
      this.waterContextData.setAbsValue({ abs, dateKey });
      this.updateStatus();
    }

    setElectricityAbsValue({ abs, dateKey }) {
      this.electicityContextData.setAbsValue({ abs, dateKey });
      this.updateStatus();
    }

    // eslint-disable-next-line class-methods-use-this
    async handleRedInput({ payload: data }, send, done) {
      try {
        if (
          !_.isObject(data) ||
          (typeof data.water !== 'number' && typeof data.electricity !== 'number')
        ) {
          throw Boom.badData(
            'Invald input, expected abs value { water?: number, electricity?: number} '
          );
        }
        if (typeof data.water === 'number') {
          this.setWaterAbsValue({ abs: data.water });
        }
        if (typeof data.electricity === 'number') {
          this.setElectricityAbsValue({ abs: data.electricity });
        }
        // broadcast the change with full state context to clients
        this.sendToCore({ payload: { data: this.state, broadcast: true } });
        done();
      } catch (err) {
        this.setError(err.message);
      }
    }

    // eslint-disable-next-line class-methods-use-this
    async handleCoreInput(message) {
      if (!message.payload) {
        return;
      }
      const { topic } = this;
      // read request response
      this.sendToCore({ topic, payload: { ...message.payload, data: this.state } });
    }
  }

  RED.nodes.registerType('kookon-usage', KookonUsage);
};
