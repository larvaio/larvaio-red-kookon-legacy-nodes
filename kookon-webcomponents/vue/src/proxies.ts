/* eslint-disable */
/* tslint:disable */
/* auto-generated vue proxies */
import { defineContainer } from './vue-component-lib/utils';

import type { JSX } from '@larva.io/k-webcomponents';




export const KookonArea = /*@__PURE__*/ defineContainer<JSX.KookonArea>('kookon-area', undefined, [
  'icon',
  'color',
  'colorModal',
  'colorInputs',
  'colorIconSmall',
  'enableSecurityQuickActions',
  'allowIndicationAutoColoring',
  'allowNodeAutoColoring',
  'hideTitles',
  'supTitle',
  'subTitle',
  'log',
  'mainTitle',
  'output',
  'request'
]);


export const KookonAreaContent = /*@__PURE__*/ defineContainer<JSX.KookonAreaContent>('kookon-area-content', undefined, [
  'color',
  'loading',
  'zoneopened',
  'ready',
  'armed',
  'alarm',
  'arm',
  'disarm'
]);


export const KookonUsage = /*@__PURE__*/ defineContainer<JSX.KookonUsage>('kookon-usage', undefined, [
  'icon',
  'color',
  'colorModal',
  'colorInputs',
  'colorIconSmall',
  'hideTitles',
  'supTitle',
  'subTitle',
  'log',
  'mainTitle',
  'output',
  'request'
]);


export const KookonUsageContent = /*@__PURE__*/ defineContainer<JSX.KookonUsageContent>('kookon-usage-content', undefined, [
  'waterMonths',
  'electrictyMonths'
]);


export const KookonUsageLog = /*@__PURE__*/ defineContainer<JSX.KookonUsageLog>('kookon-usage-log', undefined, [
  'entries'
]);

