import { Config } from '@stencil/core';
import { sass } from '@stencil/sass';
import { vueOutputTarget } from '@stencil/vue-output-target';

export const config: Config = {
  namespace: 'kookon',
  taskQueue: 'async',
  // bundles: [ ],
  autoprefixCss: true,
  buildEs5: false,
  extras: {
    cssVarsShim: false,
    dynamicImportShim: true,
    safari10: true,
    scriptDataOpts: true,
    shadowDomShim: true
  },
  plugins: [
    sass({
      includePaths: ['node_modules']
   }),
  ],
  nodeResolve: {
    browser: true,
    preferBuiltins: false
  },
  outputTargets: [
    vueOutputTarget({
      componentCorePackage: '@larva.io/k-webcomponents',
      proxiesFile: '../vue/src/proxies.ts',
    }),
    {
      type: 'dist',
      esmLoaderPath: '../loader',
      copy: [{ src: '**/*.scss' }],
    },
    {
      type: 'www',
      serviceWorker: null // disable service workers
    },
    {
      type: 'docs-readme'
    },
  ],
  testing: {
    allowableMismatchedPixels: 200,
    pixelmatchThreshold: 0.1,
  },
  preamble: '(C) Larva.io http://larva.io - Seee LICENSE.md',
  enableCache: true,
  devServer: {
    initialLoadUrl: '/src/components'
  }
};
