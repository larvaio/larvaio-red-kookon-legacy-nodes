/* eslint-disable no-underscore-dangle */
/* eslint-disable class-methods-use-this */
const moment = require('moment');
const _ = require('lodash');

// @TODO: data rotate: https://bitbucket.org/larvaio/larvaio-red-nodes/issues/5/usage-max-store-period
module.exports = class UtilityMeterData {
  /**
   *Creates an instance of UtilityMeterData.
   * @param {*} context - node context object for persistence
   */
  constructor({ context, key }) {
    this.key = key;
    this.context = context;
    // initialize the data
  }

  get months() {
    return this.context.get(`${this.key}_months`) || {};
  }

  set months(data) {
    this.context.set(`${this.key}_months`, data);
  }

  get abs() {
    return this.context.get(`${this.key}_abs`) || 0;
  }

  set abs(data) {
    this.context.set(`${this.key}_abs`, data);
  }

  /**
   * Save the absolute utility value
   *
   * @param {*} { abs, dateKey }
   */
  setAbsValue({ abs, dateKey }) {
    const _dateKey = dateKey || this.monthKey();
    const { months } = this;
    months[_dateKey] = { abs, date: _dateKey };
    this.months = months;
    if (_dateKey === this.monthKey()) {
      this.abs = abs;
    }
  }

  /**
   * Save the relative utility value
   *
   * @param {*} { rel }
   */
  setRelValue({ rel }) {
    const _dateKey = this.monthKey();
    this.abs += rel;
    const { months } = this;
    months[_dateKey] = { abs: this.abs, date: _dateKey };
    this.months = months;
  }

  /**
   * Get the absolute utility value
   *
   * @param {*} date
   * @returns
   */
  getAbsValue(date) {
    return _.get(this.months, [this.monthKey(date), 'abs'], 0);
  }

  /**
   * Get the relative utility value
   *
   * @param {*} date
   * @returns
   */
  getRelValue(date) {
    const abs = this.getAbsValue(date);
    const absBefore = this.getAbsValue(moment(date).subtract({ months: 1 }));
    return abs - absBefore;
  }

  /**
   * Get abs and rel data for all months
   *
   * @returns
   */
  getAllMonths() {
    const monthsCopy = [];
    const months = _.sortBy(
      _.map(this.months, (m) => m),
      (m) => moment(m.date, 'MM-YYYY')
    );
    _.forEach(months, (month) => {
      const obj = {
        abs: Number(month.abs).toFixed(2),
        rel: Number(this.getRelValue(moment(month.date, 'MM-YYYY'))).toFixed(2),
        date: month.date,
      };
      monthsCopy.unshift(obj);
    });
    return monthsCopy;
  }

  monthKey(date) {
    return moment(date).format('MM-YYYY');
  }

  lastMonthAbs() {
    const key = this.monthKey(moment().subtract({ months: 1 }));
    return _.get(this.months, [key, 'abs'], 0);
  }
};
