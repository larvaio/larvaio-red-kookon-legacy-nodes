/* eslint-disable no-console */
/* eslint-disable class-methods-use-this */

const OUTPUTS = 6;
const crypto = require('crypto');
const { LogLevel } = require('@larva.io/report-definitions');
const { ByErrorRecognitionBox, ByErrorRecognitionGlobal } = require('./kookon-error-codes');

module.exports = function RegisterKookonArea(RED) {
  class KookonBox {
    constructor(config) {
      this.RED = RED;
      this.RED.nodes.createNode(this, config); // this will extend with EventEmitter
      this.id = config.id;
      this.cachedLogEntris = [];
      this.boxPLCID = parseInt(config.boxPLCID, 10);
      this.plc = RED.nodes.getNode(config.plc);
      this.bySecurityStatus = this.bySecurityStatus.bind(this);
      this.sMbusOutput1 = this.sMbusOutput1.bind(this);
      this.sMbusOutput2 = this.sMbusOutput2.bind(this);
      this.arrDoors1 = this.arrDoors1.bind(this);
      this.arrDoors2 = this.arrDoors2.bind(this);
      this.filterBoxLog = this.filterBoxLog.bind(this);
      this.sMbusOutputIntervalFunc = this.sMbusOutputIntervalFunc.bind(this);
      this.err = this.err.bind(this);
      this.on('close', (done) => {
        clearInterval(this.sMbusOutputInterval);
        done();
      });
      if (this.plc && !Number.isNaN(this.boxPLCID)) {
        this.sMbusOutputInterval = setInterval(this.sMbusOutputIntervalFunc, 300000);
        this.plc.register(this);
      } else {
        this.setError('PLC config missing');
      }
    }

    get writeDefaultValues() {
      return [
        {
          tagName: `.starrStorages[${this.boxPLCID}].byStatus`,
          value: 1,
        },
        {
          tagName: `.starrStorages[${this.boxPLCID}].wAutoArmTime`,
          value: 0,
        },
      ];
    }

    get notifyHandlers() {
      return [
        {
          tagName: `.starrStorages[${this.boxPLCID}].bySecurityStatus`,
          handler: this.bySecurityStatus,
        },
        {
          tagName: `.starrStorages[${this.boxPLCID}].sMbusOutput1`,
          handler: this.sMbusOutput1,
        },
        {
          tagName: `.starrStorages[${this.boxPLCID}].sMbusOutput2`,
          handler: this.sMbusOutput2,
        },
        {
          tagName: `.starrStorages[${this.boxPLCID}].arrDoors[1].BYDOORSTATUS`,
          handler: this.arrDoors1,
        },
        {
          tagName: `.starrStorages[${this.boxPLCID}].arrDoors[2].BYDOORSTATUS`,
          handler: this.arrDoors2,
        },
        /*
        {
          tagName: `.arrLogArray`,
          handler: this.filterBoxLog,
        },
        */
      ];
    }

    bySecurityStatus(value) {
      this.output(0, value);
    }

    sMbusOutput1(value) {
      const float = parseFloat(value);
      const ret = Number.isNaN(float) ? 0 : float;
      this.sMbusOutput1Value = ret;
      this.output(2, ret);
    }

    sMbusOutput2(value) {
      const float = parseFloat(value);
      const ret = Number.isNaN(float) ? 0 : float;
      this.sMbusOutput2Value = ret;
      this.output(3, ret);
    }

    sMbusOutputIntervalFunc() {
      if (this.plc && this.plc.connected) {
        if (this.sMbusOutput1Value !== undefined) {
          this.output(2, this.sMbusOutput1Value);
        }
        if (this.sMbusOutput2Value !== undefined) {
          this.output(3, this.sMbusOutput2Value);
        }
      }
    }

    arrDoors1(value) {
      if (value >= 254) {
        // hide sliding status
        return;
      }
      this.output(4, value);
    }

    arrDoors2(value) {
      if (value >= 254) {
        // hide sliding status
        return;
      }
      this.output(5, value);
    }

    filterBoxLog(value) {
      // TODO: filter out logs that dosn't contain sHash if
      // api.kookon.ee disabled
      // eg. write hash back to device for synced items
      // and disable local cahce
      const logEntries = (value || [])
        .filter((log) => log.WBOXID === 0 || log.WBOXID === this.boxPLCID)
        .filter(
          (log) =>
            !!ByErrorRecognitionGlobal[log.BYERRORRECOGNITION] ||
            !!ByErrorRecognitionBox[log.BYERRORRECOGNITION]
        )
        .map((log) => {
          let error;
          let errorCode;
          const device = log.BYERRORRECOGNITION;
          if (log.WBOXID === 0) {
            errorCode = device === 24 || device === 25 ? 0 : log.DWERRORID;
            if (!ByErrorRecognitionGlobal[device][errorCode]) {
              errorCode = 0;
            }
            error = ByErrorRecognitionGlobal[device][errorCode];
          } else {
            if (!ByErrorRecognitionBox[device][errorCode]) {
              errorCode = 0;
            }
            errorCode = device === 6 || device === 8 ? 0 : log.DWERRORID;
            error = ByErrorRecognitionBox[device][errorCode];
          }
          error = error || { message: 'Internal Server Error', logLevel: LogLevel.ERROR };
          error.hash = crypto
            .createHash('sha1')
            .update(`${log.DTTIMESTAMP}${device}${errorCode}`)
            .digest('hex');
          return {
            ...error,
            timestamp: new Date(log.DTTIMESTAMP), // TODO? timestamp shift?
          };
        });
      const filteredLogEntries = logEntries.filter(
        (log) => !this.cachedLogEntris.find((cachedLog) => cachedLog.hash === log.hash)
      );
      this.cachedLogEntris = logEntries;
      if (filteredLogEntries.length > 0) {
        // @TODO: enable if api.kookon.ee removed.
        // this.output(1, filteredLogEntries);
      }
    }

    err(err) {
      this.setError(err.message);
    }

    setError(err) {
      this.status({ fill: 'red', shape: 'ring', text: err });
    }

    output(outputNumber, data) {
      const topic = this.boxPLCID;
      const output = new Array(OUTPUTS);
      output.fill(null, 0);
      output[outputNumber] = { topic, payload: data };
      this.send(output); // RED output
    }
  }

  RED.nodes.registerType('kookon-box-out', KookonBox);
};
