import { Component, Prop, h, Element, Event, EventEmitter, Method, State } from '@stencil/core';

@Component({
  tag: 'kookon-area',
  styleUrl: 'kookon-area.scss',
  shadow: true,
})
export class KookonAreea {
  @Element() el!: HTMLKookonAreaElement;
  /**
   * Component main icon
   */
  @Prop({ mutable: true }) icon?: string = 'security2';
  /**
   * The color to use from your application's color palette.
   * Detrouble options are: `"primary"`, `"secondary"`, `"tertiary"`, `"success"`, `"warning"`, `"danger"`, `"light"`, `"medium"`, and `"dark"`.
   */
  @Prop({ mutable: true }) color?: string = 'primary';
  /**
   * The color to use from your application's color palette for Components modal window.
   */
  @Prop({ mutable: true }) colorModal?: string;
  /**
   * The color to use from your application's color palette for inputs
   */
  @Prop({ mutable: true }) colorInputs?: string;
  /**
   * The color to use from your application's color palette for indication icon
   */
  @Prop() colorIconSmall?: string;
  /**
   * Disable quck actions
   */
  @Prop() enableSecurityQuickActions: boolean = false;
  /**
   * Allow node indication color automatic change based on feedback/node value. Defaults to false
   */
  @Prop() allowIndicationAutoColoring: boolean = false;
  /**
   * Allow node color automatic change based on feedback/node value. Defaults to true
   */
  @Prop() allowNodeAutoColoring: boolean = true;
  /**
   * Hide node titles
   */
  @Prop() hideTitles?: boolean;
  /**
   * Component superscript title
   */
  @Prop() supTitle?: string;
  /**
   * Component subtitle
   */
  @Prop() subTitle?: string;
  /**
   * Is logging for this component enabled (lar-log subcomponent loaded)
   */
  @Prop() log: boolean = false;
  /**
   * Component main title
   */
  @Prop() mainTitle!: string;
  /**
   * @see {@link ../readme.md} chapter "Components input and output" for further information.
   */
  @Event() output!: EventEmitter;
  /**
   * @see {@link ../readme.md} chapter "Sub-Components requests and responses" for further information.
   */
  @Event() request!: EventEmitter;
  /////// LarvaNode base properties and events - end
  @State() stateReceived = false;
  @State() ready = false;
  @State() alarm = false;
  @State() loading: boolean = true;
  @State() armed = false;
  @State() zoneopened = false;
  @State() troubles = [];

  private node!: any;

  constructor() {
    const larNodeDefined = customElements.get('lar-node');
    if (!larNodeDefined) {
      throw new Error('@larva.io/webcomponents are requied to use Kookon WebComponnets');
    }
  }

  componentDidLoad() {
    const el = this.el.shadowRoot || this.el;
    this.node = el.querySelector('lar-node') as any;
    this.output.emit({}); // getFullState
  }

  /**
   * Larva error input
   */
  @Method()
  async error(data: any) {
    if (this.node && this.node.error) {
      this.node.error(data);
    }
    if (this.stateReceived) {
      this.loading = false;
    }
  }

  arm() {
    this.loading = true;
    return this.output.emit({ command: 'arm' });
  }

  disarm() {
    this.loading = true;
    this.output.emit({ command: 'disarm' });
  }

  handleQuickAction() {
    if (this.loading) {
      return;
    }
    if (this.zoneopened) {
      return this.error('AREA_ARM_FAILED_INVALID_ZONE_STATE');
    }
    if (this.armed) {
      return this.disarm();
    } else {
      return this.arm();
    }
  }

  /**
   * Larva input message
   */
  @Method()
  async input(data: any) {
    this.loading = false;
    if (data && typeof data === 'object' ) {
      this.stateReceived = true;
      this.armed = data.armed === true;
      this.ready = data.ready === true;
      this.zoneopened = data.zoneopened === true;
      this.alarm = data.alarm === true;
    }
  }

  render() {
    let iconSmall = 'warning';
    if (this.ready) {
      iconSmall = this.armed === true ? 'lock' : 'unlock';
    }
    let value = this.armed === true ? 'area.armed' : 'area.disarmed';
    if (this.alarm === true) {
      value = 'area.alarms.intruder'
      iconSmall = 'intruder_alarm';
    }
    let iconSmallColor = this.colorIconSmall;
    if (this.allowIndicationAutoColoring && !this.ready) {
      iconSmallColor = 'warning';
    } else if (this.allowIndicationAutoColoring && this.alarm) {
      iconSmallColor = this.allowNodeAutoColoring ? this.colorIconSmall : 'danger'; // node is colored red, so dont use indication color or use default
    } else if (this.allowIndicationAutoColoring && this.armed) {
      iconSmallColor = 'success';
    } else {
      iconSmallColor = 'danger';
    }
    const componentProps = {
      loading: this.loading,
      color: this.colorInputs || this.color,
      zoneopened: this.zoneopened,
      ready: this.ready,
      armed: this.armed,
      alarm: this.alarm,
      onArm: () => { this.arm(); },
      onDisarm: () => { this.disarm(); }
    };
    return [
      <lar-node
        value={value}
        iconSmall={iconSmall}
        icon={this.icon}
        hideTitles={this.hideTitles}
        color={this.alarm && this.allowNodeAutoColoring === true ? 'danger' : this.color}
        supTitle={this.supTitle}
        mainTitle={this.mainTitle}
        subTitle={this.subTitle}
        colorModal={this.alarm && this.allowNodeAutoColoring === true ? 'danger' : this.colorModal}
        colorInputs={this.colorInputs}
        colorIconSmall={iconSmallColor}
        log={this.log}
        loading={this.loading}
        component="kookon-area-content"
        componentProps={componentProps}>
          {this.enableSecurityQuickActions === true &&
           <lar-button-push
              slot="titles"
              size="small"
              color={this.colorInputs || this.color}
              disabled={this.loading || !this.ready || this.zoneopened}
              onClick={() => this.handleQuickAction()}
            />
          }
      </lar-node>,
    ];
  }
}
