const Boom = require('@hapi/boom');
const { ReportEvent, Qualify } = require('@larva.io/report-definitions');
const CoreUiBaseNode = require('./core-ui-base-node');

const AREA_COMMANDS = {
  ARM: 1,
  DISARM: 9,
};

const COMMAND_TIMEOUT = 1500;

const AREA_STATUS_CODES = {
  AREA_DISARMED: 0,
  ARMED: 1,
  AREA_FORCE_ARMED: 2,
  AREA_STAY_ARMED: 3,
  AREA_INSTANT_ARMED: 4,
  AREA_EXIT_DELAY: 5,
  AREA_ENTRY_DELAY: 6,
  AREA_STROBE_ALARM: 7,
  AREA_SILENT_ALARM: 8,
  AREA_AUDIBLE_ALARM: 9,
  AREA_FIRE_ALARM: 10,
  ZONE_NOT_READY: 11,
};

module.exports = function RegisterKookonArea(RED) {
  class KookonArea extends CoreUiBaseNode {
    constructor(config) {
      super(RED, config, true);
      this.armed = false;
      this.zoneopened = false;
      this.alarm = false;
      this.ready = false;
      this.status({ fill: 'yellow', shape: 'ring', text: 'Not ready' });
    }

    getFullState() {
      return {
        ready: this.ready,
        armed: this.armed,
        alarm: this.alarm,
        zoneopened: this.zoneopened,
      };
    }

    // eslint-disable-next-line consistent-return
    async handleRedInput(message, send, done) {
      // Input is LOG
      try {
        this.ready = true;
        if (Array.isArray(message.payload)) {
          // report all logs to core
          await Promise.all(
            message.payload.map(async (log) => {
              await this.report({
                code: log.reportEvent || ReportEvent.RED_EVENT,
                qualify: log.qualify || Qualify.NEW,
              }).catch((err) => {
                // eslint-disable-next-line no-console
                console.error('Kookon Area Error reporting event', err);
              });
            })
          );
          return done();
        }
        // input is status feedback
        if (typeof message.payload === 'number') {
          switch (message.payload) {
            case AREA_STATUS_CODES.AREA_EXIT_DELAY:
            case AREA_STATUS_CODES.AREA_FORCE_ARMED:
            case AREA_STATUS_CODES.AREA_STAY_ARMED:
            case AREA_STATUS_CODES.ARMED:
              this.armed = true;
              this.alarm = false;
              this.zoneopened = false;
              this.setStatus('Armed');
              break;
            case AREA_STATUS_CODES.ZONE_NOT_READY:
              this.armed = false;
              this.alarm = false;
              this.zoneopened = true;
              this.setStatus('Zone opened');
              break;
            case AREA_STATUS_CODES.AREA_DISARMED:
            case AREA_STATUS_CODES.AREA_ENTRY_DELAY:
              this.armed = false;
              this.alarm = false;
              this.zoneopened = false;
              this.setStatus('Disarmed');
              break;
            case AREA_STATUS_CODES.AREA_STROBE_ALARM:
            case AREA_STATUS_CODES.AREA_SILENT_ALARM:
            case AREA_STATUS_CODES.AREA_AUDIBLE_ALARM:
            case AREA_STATUS_CODES.AREA_FIRE_ALARM:
              this.armed = true;
              this.alarm = true;
              this.zoneopened = false;
              await this.report({
                code:
                  message.payload === AREA_STATUS_CODES.AREA_FIRE_ALARM
                    ? ReportEvent.SECURITY_FIRE_ALARM
                    : ReportEvent.SECURITY_BURGLARY_ALARM,
                qualify: Qualify.NEW,
              }).catch((err) => {
                // eslint-disable-next-line no-console
                console.error('Kookon Area Error reporting event', err);
              });
              this.setError('Alarm');
              break;
            default:
              throw new Error('Invalid input');
          }
          const { topic } = this;
          if (this.listenerCount('fullstatechange') === 0) {
            // if we have event listener, listener boradcasts change with response
            this.core.send({ topic, payload: { broadcast: true, data: this.getFullState() } });
          } else {
            this.emit('fullstatechange');
          }
          return done();
        }
        throw new Error('Invalid input');
      } catch (err) {
        this.setError(err.message);
      }
    }

    // Core public input method
    // eslint-disable-next-line consistent-return
    async handleCoreInput(message) {
      // get full state request
      const { topic } = this;
      if (!message.payload || !message.payload.data || !message.payload.data.command) {
        return this.core.send({
          topic,
          payload: { ...message.payload, data: this.getFullState() },
        });
      }
      try {
        // eslint-disable-next-line consistent-return
        await new Promise((resolve, reject) => {
          let timeout;
          let report;
          const listener = () => {
            if (report) {
              this.report(report);
            }
            clearTimeout(timeout);
            resolve();
          };
          this.once('fullstatechange', listener);
          timeout = setTimeout(() => {
            this.removeListener('fullstatechange', listener);
            reject(Boom.badGateway('Request timeout'));
          }, COMMAND_TIMEOUT);
          const { command } = message.payload.data;
          try {
            switch (command) {
              case 'arm':
                if (this.armed === true) {
                  throw Boom.locked('Area allready armed');
                }
                if (this.zoneopened === true) {
                  throw Boom.locked('Zone opened');
                }
                report = {
                  code: ReportEvent.SECURITY_ARMED_DISARMED,
                  qualify: Qualify.RESTORE,
                  // eslint-disable-next-line no-underscore-dangle
                  user: message.payload.user,
                };
                this.send({ topic, payload: AREA_COMMANDS.ARM }); // RED output
                break;
              case 'disarm':
                if (this.armed === false) {
                  throw Boom.locked('Area allready disarmed');
                }
                report = {
                  code: ReportEvent.SECURITY_ARMED_DISARMED,
                  qualify: Qualify.NEW,
                  // eslint-disable-next-line no-underscore-dangle
                  user: message.payload.user,
                };
                this.send({ topic, payload: AREA_COMMANDS.DISARM }); // RED output
                break;
              default:
                throw Boom.badData('Invalid command input');
            }
          } catch (err) {
            this.removeListener('fullstatechange', listener);
            clearTimeout(timeout);
            reject(err);
          }
        });
        // request done, send reponse with broadcast
        this.core.send({
          topic,
          payload: { ...message.payload, data: this.getFullState(), broadcast: true },
        });
      } catch (e) {
        const err = Boom.isBoom(e)
          ? e
          : Boom.badImplementation('An internal server error occurred');
        // const topic = `iot-2/evt/errors/fmt/json`; // @TODO needs to be tested
        const payload = {
          reqId: message.payload.reqId,
          err: err.output.payload,
        };
        this.core.send({ topic, payload });
        if (!Boom.isBoom(e)) {
          // eslint-disable-next-line no-console
          console.error(e);
        }
      }
    }
  }

  RED.nodes.registerType('kookon-area', KookonArea);
};
