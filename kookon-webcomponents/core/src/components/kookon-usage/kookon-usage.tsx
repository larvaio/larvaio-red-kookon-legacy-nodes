import { Component, Prop, h, Element, Event, EventEmitter, Method, State } from '@stencil/core';
import { UtilityMeterState } from './kookon-usage-interfaces';
import moment from 'moment';

@Component({
  tag: 'kookon-usage',
  styleUrl: 'kookon-usage.scss',
  shadow: true,
})
export class KookonUsage {
  @Element() el!: HTMLKookonUsageElement;
  /**
   * Component main icon
   */
  @Prop({ mutable: true }) icon?: string = 'chart-bar2';
  /**
   * The color to use from your application's color palette.
   * Detrouble options are: `"primary"`, `"secondary"`, `"tertiary"`, `"success"`, `"warning"`, `"danger"`, `"light"`, `"medium"`, and `"dark"`.
   */
  @Prop({ mutable: true }) color?: string = 'primary';
  /**
   * The color to use from your application's color palette for Components modal window.
   */
  @Prop({ mutable: true }) colorModal?: string;
  /**
   * The color to use from your application's color palette for inputs
   */
  @Prop({ mutable: true }) colorInputs?: string;
  /**
   * The color to use from your application's color palette for indication icon
   */
  @Prop() colorIconSmall?: string;
  /**
   * Hide node titles
   */
  @Prop() hideTitles?: boolean;
  /**
   * Component superscript title
   */
  @Prop() supTitle?: string;
  /**
   * Component subtitle
   */
  @Prop() subTitle?: string;
  /**
   * Is logging for this component enabled (lar-log subcomponent loaded)
   */
  @Prop() log: boolean = false;
  /**
   * Component main title
   */
  @Prop() mainTitle!: string;
  /**
   * @see {@link ../readme.md} chapter "Components input and output" for further information.
   */
  @Event() output!: EventEmitter;
  /**
   * @see {@link ../readme.md} chapter "Sub-Components requests and responses" for further information.
   */
  @Event() request!: EventEmitter;
  /////// LarvaNode base properties and events - end
  @State() loading: boolean = true;
  @State() values: UtilityMeterState = {};

  private node!: any;

  constructor() {
    const larNodeDefined = customElements.get('lar-node');
    if (!larNodeDefined) {
      throw new Error('@larva.io/webcomponents are requied to use Kookon WebComponnets');
    }
  }

  componentDidLoad() {
    const el = this.el.shadowRoot || this.el;
    this.node = el.querySelector('lar-node') as any;
    this.output.emit({}); // getFullState
  }

  /**
   * Larva error input
   */
  @Method()
  async error(data: any) {
    if (this.node && this.node.error) {
      this.node.error(data);
    }
    this.loading = false;
  }

  /**
   * Larva input message
   */
  @Method()
  async input(data: UtilityMeterState) {
    this.loading = false;
    if (data && typeof data === 'object' && data.electricity && data.water) {
      this.values = data;
    }
  }

  render() {
    const waterMonths = this.values && this.values.water && Array.isArray(this.values.water) ? this.values.water : [];
    const electrictyMonths = this.values && this.values.electricity && Array.isArray(this.values.electricity) ? this.values.electricity : [];
    const componentProps = {
      waterMonths,
      electrictyMonths
    };
    let latestMonthWater = waterMonths.length > 0 ? waterMonths[0] : null;
    let latestMonthElectricity = electrictyMonths.length > 0 ? electrictyMonths[0] : null;
    if (latestMonthWater && latestMonthWater.date !== moment().format('MM-YYYY')) {
      latestMonthWater = null;
    }
    if (latestMonthElectricity && latestMonthElectricity.date !== moment().format('MM-YYYY')) {
      latestMonthElectricity = null;
    }
    return [
      <lar-node
        icon={this.icon}
        hideTitles={this.hideTitles}
        color={this.color}
        supTitle={this.supTitle}
        mainTitle={this.mainTitle}
        subTitle={this.subTitle}
        colorModal={this.colorModal}
        colorInputs={this.colorInputs}
        log={this.log}
        loading={this.loading}
        component="kookon-usage-content"
        componentProps={componentProps}>
          <div slot="middle">
            <div class="values">
              <div class="value elecricity">
                <lar-icon icon="electricity" />&nbsp;
                {latestMonthElectricity &&<span>{Number(latestMonthElectricity.rel).toFixed(0)}kWh</span>}
              </div>
              <div class="value water">
                <lar-icon icon="water2" />&nbsp;
                {latestMonthWater &&<span>{Number(latestMonthWater.rel).toFixed(1)}m<sup>3</sup></span>}
              </div>
            </div>
            <div class="lar-value">
              <lar-translate t="kookon.currentMonth" />
            </div>
          </div>
      </lar-node>
    ];
  }
}
