const { ReportEvent, getEventMessage, LogLevel } = require('@larva.io/report-definitions');

// errors if we have box ID defined
// byErrorRecognition.dwErrorID
const ByErrorRecognitionBox = {
  1: {
    0: {
      message: 'Invalid PIN',
      reportEvent: ReportEvent.PIN_USER_AUTHORIZATION_FAILURE_INVALID_PIN,
      reportEventMessage: getEventMessage(ReportEvent.PIN_USER_AUTHORIZATION_FAILURE_INVALID_PIN),
      logLevel: LogLevel.ERROR,
    },
    1: {
      message: 'PIN entering timeout',
      reportEvent: ReportEvent.PIN_USER_AUTHORIZATION_FAILURE_INVALID_PIN,
      reportEventMessage: getEventMessage(ReportEvent.PIN_USER_AUTHORIZATION_FAILURE_INVALID_PIN),
      logLevel: LogLevel.ERROR,
    },
    2: {
      message: 'too many wrong pins entered',
      reportEvent: ReportEvent.LOCAL_USER_LOGIN_INPUT_DISABLED_BECAUSE_OF_INVALID_ATTEMPTS,
      reportEventMessage: getEventMessage(
        ReportEvent.LOCAL_USER_LOGIN_INPUT_DISABLED_BECAUSE_OF_INVALID_ATTEMPTS
      ),
      logLevel: LogLevel.ERROR,
    },
    3: {
      message: 'too many unauthorised access tries',
      reportEvent: ReportEvent.LOCAL_USER_LOGIN_INPUT_DISABLED_BECAUSE_OF_INVALID_ATTEMPTS,
      reportEventMessage: getEventMessage(
        ReportEvent.LOCAL_USER_LOGIN_INPUT_DISABLED_BECAUSE_OF_INVALID_ATTEMPTS
      ),
      logLevel: LogLevel.ERROR,
    },
    4: { message: 'card teaching timeout', logLevel: LogLevel.WARNING },
    5: { message: 'pinpad pressed while teaching mode is active', logLevel: LogLevel.WARNING },
    6: { message: 'card is taught but disabled', logLevel: LogLevel.WARNING },
    7: { message: 'no free space to add normal user card', logLevel: LogLevel.WARNING },
    8: { message: 'wrong card type (not desfire)', logLevel: LogLevel.WARNING },
    9: { message: 'card successfully teached', logLevel: LogLevel.WARNING },
    10: {
      message: 'error with sending sms',
      logLevel: LogLevel.ERROR,
    },
    11: { message: 'no room to add Admin card', logLevel: LogLevel.WARNING },
  },
  2: {
    0: { message: 'Alarm (bell) is active', logLevel: LogLevel.ERROR },
  },
  3: {
    0: { message: 'Security activated', logLevel: LogLevel.ERROR },
  },
  4: {
    0: { message: 'TX Block error', logLevel: LogLevel.ERROR },
  },
  5: {
    0: { message: 'Door block error', logLevel: LogLevel.ERROR },
  },
  6: {
    // Here we have exception: dwErrorID = 10 = pinpad addr 0 at module 1
    0: { message: 'No communication with pinpad', logLevel: LogLevel.ERROR },
  },
  7: {
    0: { message: 'Automatic Arming', logLevel: LogLevel.INFO },
  },
  8: {
    // Here we have exception: dwErrorID = 10 = pinpad addr 0 at module 1
    0: { message: 'No communication with second pinpad', logLevel: LogLevel.ERROR },
  },
  9: {
    0: { message: 'Door block nr 2 error', logLevel: LogLevel.ERROR },
  },
  10: {
    0: { message: 'TX block nr 2 error', logLevel: LogLevel.ERROR },
  },
};

// Global errors, if dwboxid=0
const ByErrorRecognitionGlobal = {
  1: {
    0: { message: 'PLC Power Fail', logLevel: LogLevel.ERROR },
  },
  2: {
    0: { message: 'RS485 module 1 com error', logLevel: LogLevel.ERROR },
  },
  3: {
    0: { message: 'RS485 module 2 com error', logLevel: LogLevel.ERROR },
  },
  4: {
    0: { message: 'RS485 module 3 com error', logLevel: LogLevel.ERROR },
  },
  5: {
    0: { message: 'Pinpad RX block 1 error', logLevel: LogLevel.ERROR },
  },
  6: {
    0: { message: 'Pinpad RX block 2 error', logLevel: LogLevel.ERROR },
  },
  7: {
    0: { message: 'Pinpad RX block 3 error', logLevel: LogLevel.ERROR },
  },
  8: {
    0: { message: 'Pinpad TX block 1 error', logLevel: LogLevel.ERROR },
  },
  9: {
    0: { message: 'Pinpad TX block 2 error', logLevel: LogLevel.ERROR },
  },
  10: {
    0: { message: 'Pinpad TX block 3 error', logLevel: LogLevel.ERROR },
  },
  11: {
    0: { message: 'MBus Control block error', logLevel: LogLevel.ERROR },
  },
  12: {
    0: { message: 'Ups battery discharge', logLevel: LogLevel.ERROR },
  },
  13: {
    0: { message: 'Ups battery fail', logLevel: LogLevel.ERROR },
  },
  14: {
    0: { message: 'Sensors short circuit', logLevel: LogLevel.ERROR },
  },
  15: {
    0: { message: 'Sensors power fail', logLevel: LogLevel.ERROR },
  },
  16: {
    0: { message: 'Doors power fail', logLevel: LogLevel.ERROR },
  },
  17: {
    0: { message: 'Doors short circuit', logLevel: LogLevel.ERROR },
  },
  18: {
    0: { message: 'Bells power fail', logLevel: LogLevel.ERROR },
  },
  19: {
    0: { message: 'Bells short circuit', logLevel: LogLevel.ERROR },
  },
  20: {
    0: { message: 'Global power fail', logLevel: LogLevel.ERROR },
  },
  21: {
    0: { message: 'Global short circuit', logLevel: LogLevel.ERROR },
  },
  22: {
    0: { message: 'Backup logging error (log file)', logLevel: LogLevel.ERROR },
  },
  23: {
    0: { message: 'SMS send failed', logLevel: LogLevel.ERROR },
    1: { message: 'DNS error', logLevel: LogLevel.ERROR },
    2: { message: 'HTTP Get error', logLevel: LogLevel.ERROR },
    3: { message: 'SMS send failed', logLevel: LogLevel.ERROR },
    4: { message: 'Timeout', logLevel: LogLevel.ERROR },
  },
  24: {
    // exception
    /*
      K-Bus error
    errorcodes(dwErrorID):  Bit 0 = K-Bus Err
    Bit 1 = Terminal State Err
    Bit 2 = Process Data Length Err
    Bit 8 = No valid Inputs
    Bit 9 = K-Bus Input Update busy
    Bit 10 = K-Bus Output Update busy
    Bit 11 = Watchdog Err
    Bit 15 = Acyc. Function atcive (e.g. K-Bus Reset)
      */
    0: { message: 'Interconnection error', logLevel: LogLevel.ERROR },
  },
  25: {
    // exception Beckhoff ADS Error Codes
    0: { message: 'Trap Sending Error', logLevel: LogLevel.ERROR },
  },
};

module.exports = {
  ByErrorRecognitionBox,
  ByErrorRecognitionGlobal,
};
