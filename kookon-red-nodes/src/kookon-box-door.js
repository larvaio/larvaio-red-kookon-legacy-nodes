/* eslint-disable class-methods-use-this */

module.exports = function RegisterKookon(RED) {
  class Kookon {
    constructor(config) {
      this.RED = RED;
      this.RED.nodes.createNode(this, config); // this will extend with EventEmitter
      this.id = config.id;
      this.boxPLCID = parseInt(config.boxPLCID, 10);
      this.doorID = parseInt(config.doorID, 10);
      this.plc = RED.nodes.getNode(config.plc);
      if (this.plc && !Number.isNaN(this.boxPLCID) && !Number.isNaN(this.doorID)) {
        this.plc.register(this);
      } else {
        this.setError('PLC config invalid/missing');
      }
      this.on('input', this.handleInput.bind(this));
    }

    async handleInput(msg, send, done) {
      try {
        const cmd = msg.payload;
        if (!this.plc || !this.boxPLCID) {
          throw new Error('Not Connected');
        } else {
          await this.plc.write(
            `.starrStorages[${this.boxPLCID}].arrDoors[${this.doorID}].BYDOORCONTROL`,
            0
          );
          await this.plc.write(
            `.starrStorages[${this.boxPLCID}].arrDoors[${this.doorID}].BYDOORCONTROL`,
            cmd === true ? 1 : 2
          ); // 1 = open , 2 = close
          this.status({ fill: 'green', shape: 'dot', text: 'Success' });
        }
        done();
      } catch (err) {
        this.setError(err.message);
        done(err);
      }
    }

    setError(err) {
      this.status({ fill: 'red', shape: 'ring', text: err });
    }
  }

  RED.nodes.registerType('kookon-box-door', Kookon);
};
