const Boom = require('@hapi/boom');
const _ = require('lodash');
const CoreMock = require('./core-mock');

module.exports = class CoreBaseNode {
  constructor(RED, config) {
    this.id = config.id;
    this.RED = RED;
    this.RED.nodes.createNode(this, config); // this will extend with EventEmitter
    this.on('input', this.handleRedInput.bind(this));
    // register to core
    if (process.env.NODE_ENV === 'test') {
      // eslint-disable-next-line no-console
      console.warn('###### RUNNING TEST ENV, USING CORE CONNECTION MOCK ######## ');
      this.core = new CoreMock();
    } else {
      this.core = RED.httpAdmin.get('larva-core');
      if (!this.core) {
        throw new Error('Larva nodes require red to have Larva Core module installed');
      }
    }
  }

  // eslint-disable-next-line class-methods-use-this
  handleRedInput(/* message, send, done */) {
    throw new Error('handleRedInput must be overridden');
  }

  setError(err) {
    this.status({ fill: 'red', shape: 'ring', text: err });
  }

  setStatus(text) {
    this.status({ fill: 'green', shape: 'ring', text });
  }

  /**
   *
   * @param {*} larva message
   */
  sendToCore(message) {
    if (!_.has(message, 'payload')) {
      throw Boom.badData();
    }
    const msgType = _.get(this.control, 'output') === true ? 'cmd' : 'evt';
    const topic = `iot-2/${msgType}/${this.id}/fmt/json`;
    this.core.send({ topic, payload: message.payload }).catch((e) => {
      const err = Boom.isBoom(e) ? e : Boom.badImplementation('An internal server error occurred');
      if (!Boom.isBoom(e)) {
        // eslint-disable-next-line no-console
        console.error(e);
      }
      this.setError(err.message);
    });
  }

  async report({ code, qualify, user }) {
    if (!this.core) {
      throw new Error('Larva nodes require red to have Larva Core module installed');
    }
    return this.core.report({
      code,
      qualify,
      data: { user },
      nodeId: this.id,
    });
  }
};
